# ladoyle_OptimizingCompiler
UC Irvine project class CS 142B -> an Optimizing Compiler

A compiler built as part of a project course to produce executable code for the Crux programming language set. The compiler uses a hand built recursive descent parser to generate an SSA intermediate representation of the code.
