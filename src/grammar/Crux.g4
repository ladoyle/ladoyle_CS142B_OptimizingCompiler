grammar Crux;
program
 : declarationList EOF
 ;

declarationList
 : declaration*
 ;

declaration
 : variableDeclaration
 | arrayDeclaration
 | functionDefinition
 ;

variableDeclaration
 : Var Identifier ':' type ';'
 ;

type
 : Identifier
 ;

SemiColon: ';';
Colon: ':';

Open_paren: '(';
Close_paren: ')';
Open_brace: '{';
Close_brace: '}';
Open_bracket: '[';
Close_bracket: ']';
Add: '+';
Sub: '-';
Mul: '*';
Div: '/';
Greater_equal: '>=';
Lesser_equal: '<=';
Not_equal: '!=';
Equal: '==';
Greater_than: '>';
Less_than: '<';
Assign: '=';
Comma: ',';
Call: '::';

Integer
 : '0'
 | [1-9] [0-9]*
 ;

Var: 'var';

And: 'and';
Or: 'or';
Not: 'not';
Let: 'let';
Array: 'array';
Func: 'func';
If: 'if';
Else: 'else';
While: 'while';
True: 'true';
False: 'false';
Return: 'return';

Identifier
 : [a-zA-Z] [a-zA-Z0-9_]*
 ;

WhiteSpaces
 : [ \t\r\n]+ -> skip
 ;

Comment
 : '//' ~[\r\n]* -> skip
 ;

literal: Integer | True | False ;

designator: Identifier ( '[' expression0 ']' )* ;

op0: '>=' | '<=' | '!=' | '==' | '>' | '<' ;
op1: '+' | '-' | Or ;
op2: '*' | '/' | And ;

expression0: expression1 ( op0 expression1 )? ;
expression1: expression2 ( op1  expression2 )* ;
expression2: expression3 ( op2 expression3 )* ;
expression3: Not expression3
       | '(' expression0 ')'
       | designator
       | callExpression
       | literal ;
callExpression: '::' Identifier '(' expressionList ')' ;
expressionList: ( expression0 ( ',' expression0 )* )? ;

parameter: Identifier ':' type ;
parameterList: ( parameter ( ',' parameter )* )? ;

arrayDeclaration: Array Identifier ':' type '[' Integer ']' ( '[' Integer ']' )* ';' ;
functionDefinition: Func Identifier '(' parameterList ')' ':' type statementBlock ;

assignmentStatement: Let designator '=' expression0 ';' ;
callStatement: callExpression ';' ;
ifStatement: If expression0 statementBlock ( Else statementBlock )? ;
whileStatement: While expression0 statementBlock ;
returnStatement: Return expression0 ';' ;
statement: variableDeclaration
           | callStatement
           | assignmentStatement
           | ifStatement
           | whileStatement
           | returnStatement ;
statementList: ( statement )* ;
statementBlock: '{' statementList '}' ;

