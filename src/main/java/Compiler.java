/**
 * Compiler: class
 *  main for the program
 */
public class Compiler {

    // starts a new driver if args are correct
    public static void main(String[] args) {
        if(args.length == 2) {
            Driver driver = new Driver(args[1]);
            var result = driver.run();
            if(result == State.Failed) System.exit(-1);
        }
        else printUsage();
    }

    // prints command line usage
    private static void printUsage() {
        System.out.println("\nUsage: compiler <CruxFileToCompile>\n");
    }
}
