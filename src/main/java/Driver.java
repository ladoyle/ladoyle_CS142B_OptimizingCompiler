import frontend.Lexer;
import frontend.Parser;
import frontend.util.GraphPrinter;
import frontend.util.SSAgraph;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 * State: enum
 *  tracks state of compiler
 *  -> Continue: finished stage, move to next
 *  -> Finished: compilation finished
 *  -> Failed: error occurred during compilation
 */
enum State {
    Continue,
    Finished,
    Failed;

    // checks if compilation state is Finished
    State complete() { return this == Finished ? Finished : Failed; }

    State then(Supplier<State> nextStage) {
        if(this == Continue)
            return nextStage.get();
        return this;
    }
}

/**
 * Driver: class
 *  runs all stages of compilation
 *  -> tokenizing by lexer
 *  -> parsing by recursiveDescentParser
 *
 *  can be set to run individual stages
 */
public class Driver {
    private boolean tokenize = false;
    private boolean parse = false;

    private final String filename;
    private ArrayList<String> tokens;

    // sets name of file to be compiled
    public Driver(String arg) {
        this.filename = arg;
        this.tokens = new ArrayList<>();
    }

    // runs all desired stages of compilation
    public State run() {
        return tokenize()
                .then(this::parse)
                .complete();
    }

    // lexer stage:
    // creates a frontend.Lexer and tokenizes a given file
    private State tokenize() {
        Lexer lexer = new Lexer(filename);
        tokens = lexer.tokenize();
        if(lexer.errorOccurred()) {
            for(String msg : lexer.getErrorMessages()) {
                System.err.println(String.format("%s", msg));
            }
            return State.Failed;
        }
        if(tokenize) {
            for(var token : tokens)
                System.out.println(token);
            return State.Finished;
        }
        return State.Continue;
    }

    // parser stage:
    // creates a frontend.Parser and parses token list
    private State parse() {
        Parser parser = new Parser(tokens);
        SSAgraph graph = parser.parse();
        if(parser.errorOccurred()) {
            for(String msg : parser.getErrorMessages()) {
                System.err.println(String.format("%s", msg));
            }
            return State.Failed;
        }
        if(parse) {
            GraphPrinter graphPrinter = new GraphPrinter(graph);
            graphPrinter.print(System.out);
            return State.Finished;
        }
        return State.Continue;
    }

    // enable lexer stage
    public void setLexer() { this.tokenize = true; }
    // enable parser stage
    public void setParser() { this.parse = true; }
}
