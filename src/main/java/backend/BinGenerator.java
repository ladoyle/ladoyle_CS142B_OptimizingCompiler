package backend;

import java.util.ArrayList;
import java.util.List;

public class BinGenerator {
    private static final int REGISTER_LENGTH = 32;
    private int pc;
    private List<Integer> buf;

    BinGenerator() {
        pc = -1;
        buf = new ArrayList<>();
    }

    public void putF1(int op, int a, int b, int c) {
        buf.add(op << 26 | a << 21 | b << 16 | c);
        ++pc;
    }

    public void putF2(int op, int a, int b, int c) {
        buf.add(op << 26 | a << 21 | b << 16 | c);
        ++pc;
    }

    public void putF3(int op, int c) {
        buf.add(op << 26 | c);
        ++pc;
    }
}
