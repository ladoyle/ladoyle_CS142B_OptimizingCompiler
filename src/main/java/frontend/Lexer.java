package frontend;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * backend.Lexer: class
 *  tokenizes a file into usable Symbols for a parser
 *  uses the Crux specification in src.grammar.Crux.g4 to determine tokens
 */
public class Lexer {
    private int row;
    private int col;
    private final String filename;
    private final ArrayList<String> tokens;
    private final List<String> errorMessages;
    private byte[] fileData;

    public Lexer(String newFile) {
        this.row = 1;
        this.col = 1;
        this.errorMessages = new ArrayList<>();
        this.tokens = new ArrayList<>();
        this.fileData = new byte[0];
        this.filename = newFile;
    }

    public ArrayList<String> tokenize() {
        fileData = readFile();
        for(int i = 0; i < fileData.length; ++i) {
            String atom = String.valueOf((char)fileData[i]);
            if(atom.matches("[a-zA-Z]")) {
                i += tokenizeIdentifier(i);
                continue;
            }
            else if(atom.matches("[1-9]")) {
                i += tokenizeDigit(i);
                continue;
            }
            else if(atom.matches("[0]")) {
                if(i + 1 < fileData.length) {
                    if(!isWhiteSpace(fileData[i + 1]) && !isSymbol((char)fileData[i + 1])) {
                        this.errorMessages.add(
                                "Syntax Error: Digit contains illegal character at " + row +
                                        ":" + col
                        );
                        continue;
                    }
                }
                ++col;
                tokens.add(atom);
                continue;
            }
            else if(isSymbol((char)fileData[i])) {
                i += tokenizeSymbol(i);
                continue;
            }
            else if(isWhiteSpace(fileData[i]))
                continue;
            ++col;
            this.errorMessages.add(
                    "Syntax Error: Illegal character at " + row + ":" + col
            );
        }
        return tokens;
    }

    private int tokenizeIdentifier(int index) {
        int start = index;
        for(++index; index < fileData.length; ++index) {
            String atom = String.valueOf((char)fileData[index]);
            ++col;
            if(isWhiteSpace(fileData[index]) || isSymbol((char)fileData[index])) break;
            else if(!atom.matches("[a-zA-Z0-9_]")) {
                this.errorMessages.add(
                        "Syntax Error: Identifier contains illegal character at " +
                                row + ":" + col
                );
                return index - start;
            }
        }
        byte[] buf = Arrays.copyOfRange(fileData, start, index);
        tokens.add(new String(buf));
        return index - start - 1;
    }

    private int tokenizeDigit(int index) {
        int start = index;
        for(++index; index < fileData.length; ++index) {
            String atom = String.valueOf((char)fileData[index]);
            ++col;
            if(isWhiteSpace(fileData[index]) || isSymbol((char)fileData[index])) break;
            else if(!atom.matches("[0-9]")) {
                this.errorMessages.add(
                        "Syntax Error: Digit contains illegal character at " +
                                row + ":" + col
                );
                return index - start;
            }
        }
        byte[] buf = Arrays.copyOfRange(fileData, start, index);
        tokens.add(new String(buf));
        return index - start - 1;
    }

    private int tokenizeSymbol(int index) {
        char next;
        int start = index++;

        switch ((char)fileData[start]) {
            case ':':
                if(index < fileData.length) {
                    next = (char)fileData[index];
                    if(next == ':') {
                        ++col;
                        ++index;
                    }
                }
                break;
            case '<':
            case '>':
            case '!':
            case '=':
                if(index < fileData.length) {
                    next = (char)fileData[index];
                    if(next == '=') {
                        ++col;
                        ++index;
                    }
                }
                break;
            case '/':
                if(index < fileData.length) {
                    next = (char)fileData[index];
                    if(next == '/') {
                        ++row;
                        col = 1;
                        while(index < fileData.length && (char)fileData[index] != '\n')
                            ++index;
                        return index - start;
                    }
                }
        }
        byte[] buf = Arrays.copyOfRange(fileData, start, index);
        tokens.add(new String(buf));
        return index - start - 1;
    }

    private boolean isWhiteSpace(byte atom) {
        boolean isWhite = String.valueOf((char)atom).matches("[ \t\r\n]");
        if('\n' == (char)atom) {
            row += 1;
            col = 1;
        }
        else if(isWhite) ++col;
        return isWhite;
    }

    private boolean isSymbol(char atom) {
        char[] symbols = {
                ',', ';', ':',
                '(', ')', '{', '}', '[', ']',
                '+', '-', '*', '/',
                '>', '<', '!', '='
        };

        for(char sym : symbols) {
            if(atom == sym)
                return true;
        }
        return false;
    }

    private byte[] readFile() {
        byte[] buf = new byte[0];
        var loader = getClass().getClassLoader();
        try (var fileStream = Objects.requireNonNull(loader.getResourceAsStream(filename))){
            buf = fileStream.readAllBytes();
            return buf;
        }
        catch (IOException e) {
            errorMessages.add(e.getMessage());
            return buf;
        }
    }

    public boolean errorOccurred() {
        return !errorMessages.isEmpty();
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }
}