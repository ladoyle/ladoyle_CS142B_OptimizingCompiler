package frontend;

import frontend.util.*;

import java.util.*;

public class Parser {
    private final List<String> tokens;
    private final List<String> errorMessages;
    private final Stack<Integer> mJoinBlocks;
    private final Stack<Map<String, Integer>> mSymTables;
    private final Stack<Map<String, Instruction>> mVarInstMaps;
    private Symbol sym;
    private int mCondOp = 0;
    private final SSAgraph mGraph;

    public Parser(ArrayList<String> tokens) {
        this.mSymTables = new Stack<>(){
            {
                push(new HashMap<>(){
                    {
                        put("printInt", -1);
                        put("printBool", -1);
                        put("readInt", -1);
                        put("println", -1);
                    }
                });
            }
        };
        this.mGraph = new SSAgraph();
        this.tokens = tokens;
        this.mJoinBlocks = new Stack<>();
        this.mVarInstMaps = new Stack<>();
        this.errorMessages = new ArrayList<>();
    }

    public SSAgraph parse() {
        mGraph.addBlock(1);
        while(!tokens.isEmpty()) {
            declaration();
        }
        return mGraph;
    }

    private void declaration() {
        next();
        switch (sym.getSym()) {
            case "var":
                variableDeclaration();
                break;
            case "array":
                arrayDeclaration();
                break;
            case "func":
                functionDefinition();
                break;
            default:
                declarationError();
                break;
        }
    }

    private void variableDeclaration() {
        next();
        if(!mSymTables.peek().containsKey(sym.getSym())) {
            newSymbolTableEntry();
            next();
            if(sym.getSym().equals(":")) {
                next();
                type();
                if (sym.getSym().equals(";")) next();
                else variableError();
            } else variableError();
        } else variableError();
    }

    private void arrayDeclaration() {
        next();
    }

    private void functionDefinition() {
        next();
        newSymbolTableEntry(); next();
        next();
        parameterList();
        next(); next();
        type();
        statementBlock();
    }

    private void parameterList() {
        while(!sym.getSym().equals(")")) next();
    }

    private void statementBlock() {
        next();
        statementList();
        next();
    }

    private void statementList() {
        while(!sym.getSym().equals("}")) statement();
    }

    private void statement() {
        String current = sym.getSym();
        switch (current) {
            case "var": variableDeclaration(); break;
            case "::": callStatement(); break;
            case "let": assignmentStatement(); break;
            case "if": ifStatement(); break;
            case "while": whileStatement(); break;
            case "return": returnStatement();
        }
    }

    private void returnStatement() {
        next();
        expression0();
        next();
    }

    private void whileStatement() {
        next();
        expression0();
        statementBlock();
    }

    private void ifStatement() {
        mVarInstMaps.push(new HashMap<>());
        mSymTables.push(new HashMap<>());
        next();
        expression0();

        // add the branch instruction and connect the next two instructions
        int inst_id = mGraph.getInstID() + 1;
        mGraph.addInstruction(
                mCondOp,
                // inst_id + 2 is the index of the instruction to branch to
                new Result(Result.ValueKind.Variable, inst_id + 2),
                null
        );
        mGraph.getInstruction(inst_id).insertInstruction(inst_id + 1);
        mGraph.getInstruction(inst_id).insertInstruction(inst_id + 2);

        // add new blocks and move to fall-through block
        mGraph.addBlock(3);
        mJoinBlocks.push(mGraph.getBlockID() + 2);
        statementBlock();
        mSymTables.pop();

        // add bra instruction to fall-through and connect to joinBlock
        int bra_target = inst_id + 3;
        mGraph.addInstruction(
                39,
                new Result(Result.ValueKind.Variable, bra_target),
                null
        );
        inst_id = mGraph.getInstID();
        mGraph.getInstruction(inst_id).insertInstruction(bra_target);
        mGraph.getBlock(mGraph.getBlockID()).connectBlock(mJoinBlocks.peek());

        mGraph.switchBlock(mGraph.getBlockID() + 1);
        if(sym.getSym().equals("else")) {
            mSymTables.push(new HashMap<>());
            next();
            statementBlock();
        }
        // connect branch block to joinBlock
        int temp_id = mGraph.getInstID();
        if(temp_id > inst_id)
            mGraph.getInstruction(temp_id).insertInstruction(bra_target);
        else
            mGraph.getInstruction(bra_target - 1).insertInstruction(bra_target);
        mGraph.getBlock(mGraph.getBlockID()).connectBlock(mJoinBlocks.peek());
        mGraph.switchBlock(mJoinBlocks.peek());

        // change scope
        mSymTables.pop();
        mJoinBlocks.pop();
        mVarInstMaps.pop();
    }

    private void assignmentStatement() {
        next();
        Result des = designator();
        String varName = sym.getSym(); next();
        next();
        Result val = expression0();
        next();
        if(mSymTables.size() > 1) {
            // look only at the top of the instructions stack (this is the current nesting level)
            var varInstMap = mVarInstMaps.peek();
            if(varInstMap.containsKey(varName)) {
                // modify existing phi instruction
                var inst = varInstMap.get(varName);
                mGraph.changeInstruction(
                        1, inst, 13,
                        null, new Result(Result.ValueKind.Variable, val.getNum())
                );
            } else {
                // create new phi instruction in current joinBlock
                int temp_block = mGraph.getBlockID();
                mGraph.switchBlock(mJoinBlocks.peek());

                // add phi instruction
                Result phi = computeResult(13, val, des);
                mVarInstMaps.peek().put(varName, mGraph.getInstruction(phi.getNum()));

                // add result of phi and return to current block
                addSymbolEntry(true, varName, phi.getNum());
                mGraph.switchBlock(temp_block);
            }
        }
        addSymbolEntry(false, varName, val.getNum());
    }

    private void callStatement() {
        callExpression();
    }

    private Result callExpression() {
        next();
        // branching required here
        next();
        next();
        expressionList();
        next();
        // placeholder return statement
        return new Result(Result.ValueKind.Register, 0);
    }

    private void expressionList() {
        if(sym.getSym().equals(")")) return;
        expression0();
        while(!sym.getSym().equals(")")) {
            next();
            expression0();
        }
    }

    private Result expression0() {
        Result x = expression1();
        int op = op0();
        if(op != -1) {
            Result y = expression1();
            x = computeResult(5, x, y);
            mCondOp = op;
        }
        return x;
    }

    private Result expression1() {
        Result x = expression2();
        int op = op1();
        while(op != -1) {
            Result y = expression2();
            x = computeResult(op, x, y);
            op = op1();
        }
        return x;
    }

    private Result expression2() {
        Result x = expression3();
        int op = op2();
        while(op != -1) {
            Result y = expression3();
            x = computeResult(op, x, y);
            op = op2();
        }
        return x;
    }

    private Result expression3() {
        String current = sym.getSym();
        Result x;
        switch (current) {
            case "not": next();
                x = expression3();
                x = computeResult(12, x, null);
                break;
            case "(": next(); x = expression0(); next(); break;
            case "::": x = callExpression(); break;
            default:
                if(current.matches("^[0-9]|true|false")) x = literal();
                else {
                    x = designator();
                    next();
                }
        }
        return x;
    }

    private Result literal() {
        String current = sym.getSym();
        next();
        switch (current) {
            case "true":
                return new Result(Result.ValueKind.Constant, 1);
            case "false":
                return new Result(Result.ValueKind.Constant, 0);
            default:
                Result res = new Result(
                        Result.ValueKind.Constant, Integer.parseInt(current)
                );
                return computeResult(11, res, null);
        }
    }

    private Result designator() {
        int val = lookupValue(sym.getSym());
        // TODO: arrays are a major pain here
//        next();
//        while (sym.getSym().equals("[")) {
//            next();
//            expression0();
//            next();
//        }
        return new Result(Result.ValueKind.Variable, val);
    }

    private int op0() {
        String current = sym.getSym();
        switch(current) {
            case ">=": next(); return 42;
            case "<=": next(); return 45;
            case "!=": next(); return 40;
            case "==": next(); return 41;
            case ">": next(); return 44;
            case "<": next(); return 43;
            default: return -1;
        }
    }

    private int op1() {
        String current = sym.getSym();
        switch (current) {
            case "+": next(); return 0;
            case "-": next(); return 1;
            case "or": next(); return 8;
            default: return -1;
        }
    }

    private int op2() {
        String current = sym.getSym();
        switch(current) {
            case "*": next(); return 2;
            case "/": next(); return 3;
            case "and": next(); return 9;
            default: return -1;
        }
    }

    private Result computeResult(int op, Result x, Result y) {
        int inst_id = mGraph.instructionExists(
                mGraph.getTempInstruction(op, x, y)
        );
        if(inst_id == -1) return new Result(
                Result.ValueKind.Variable,
                mGraph.addInstruction(op, x, y)
        );
        else return new Result(
                    Result.ValueKind.Variable, inst_id
            );
    }

    private void type() {
        next();
    }

    private int lookupValue(String varName) {
        int i = mSymTables.size() - 1;
        Integer value = null;
        while(value == null && i >= 0) {
            value = mSymTables.elementAt(i).get(varName);
            --i;
        }
        if(value == null) return -1;
        else return value;
    }

    private void newSymbolTableEntry() {
        mSymTables.peek().put(sym.getSym(), -1);
    }

    private void addSymbolEntry(boolean join, String name, int val) {
        if(join) {
            var table = mSymTables.pop();
            mSymTables.peek().put(name, val);
            mSymTables.push(table);
        }
        else mSymTables.peek().put(name, val);
    }

    private void variableError() {
        errorMessages.add(
                "Parser error: variable declaration contains invalid token -> " + sym.getSym()
        );
        next();
    }

    private void declarationError() {
        errorMessages.add(
            "Parser error: declaration contains invalid token " + sym.getSym()
        );
        next();
    }

    private void next() {
        if(tokens.size() > 0) {
            sym = new Symbol(tokens.get(0), 0, 0);
            tokens.remove(0);
        }
    }

    public boolean errorOccurred() {
        return !errorMessages.isEmpty();
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }
}
