package frontend.util;

import java.util.ArrayList;
import java.util.List;

public class BasicBlock {
    private final boolean join;
    private final ArrayList<Integer> inst_ids;
    private final ArrayList<Integer> block_ids;

    public BasicBlock(boolean isJoin) {
        this.join = isJoin;
        this.inst_ids= new ArrayList<>();
        this.block_ids = new ArrayList<>();
    }

    public void connectBlock(int id) {
        block_ids.add(id);
    }

    public void insertInstruction(int id) {
        inst_ids.add(id);
    }

    public ArrayList<Integer> getBlocks() {
        return this.block_ids;
    }

    public boolean isJoin() {
        return join;
    }

    public int getFirstInstruction() {
        if(inst_ids.isEmpty())
            return -1;
        return this.inst_ids.get(0);
    }

    public List<Integer> getInstructions() {
        return this.inst_ids.subList(1, inst_ids.size());
    }
}
