package frontend.util;

import java.io.PrintStream;

public class GraphPrinter {
    private static final int SPACE_INDENT = 4;
    private final StringBuilder graphString;
    private final StringBuilder branchString;
    private final StringBuilder domString;
    private final SSAgraph mGraph;
    private final int numBlocks;

    public GraphPrinter(SSAgraph graph) {
        this.graphString = new StringBuilder();
        this.branchString = new StringBuilder();
        this.domString = new StringBuilder();
        this.mGraph = graph;
        this.numBlocks = mGraph.getNumBlocks();
        beginGraph();
        traverseGraph();
        endGraph();
    }

    private void traverseGraph() {
        for(int i = 0; i < numBlocks; ++i) {
            BasicBlock block = mGraph.getBlock(i);
            addBlock(i + 1);
            int lastInstID = block.getFirstInstruction();
            addInstruction(lastInstID);
            for(int inst : block.getInstructions()) {
                graphString.append("|");
                addInstruction(inst);
                lastInstID = inst;
            }
            connectBlocks(block, i + 1, lastInstID);
            graphString.append("}\"];");
        }
    }

    private void connectBlocks(BasicBlock currentBlock, int currentID, int instID) {
        var blockIDs = currentBlock.getBlocks();
        if(blockIDs.size() > 1) {
            for(int i = 0; i < blockIDs.size(); ++i) {
                connectBranches(i, currentID, blockIDs.get(i) + 1);
                connectDoms(currentID, blockIDs.get(i) + 1);
            }
        } else if(blockIDs.size() == 1){
            if(mGraph.getInstruction(instID).isBraInstruction())
                branch(currentID, blockIDs.get(0) + 1);
            else fallThrough(currentID, blockIDs.get(0) + 1);
        }
    }

    private void connectBranches(int type, int current, int connID) {
        switch (type) {
            case 0: branchString.append("\n"); fallThrough(current, connID); break;
            case 1: branch(current, connID); break;
            default:
        }
    }

    private void fallThrough(int current, int connID) {
        branchString.append(String.format(
                "\nbb%d:s -> bb%d:n [label=\"fall-through\"];", current, connID
        ));
    }

    private void branch(int current, int connID) {
        branchString.append(String.format(
                "\nbb%d:s -> bb%d:n [label=\"branch\"];", current, connID
        ));
    }

    private void connectDoms(int current, int connID) {
        domString.append(String.format(
                "\nbb%d:b -> bb%d:b [color=blue, style=dotted, label=\"dom\"];",
                current, connID
        ));
    }

    private void addBlock(int id) {
        graphString.append("\n").append(" ".repeat(SPACE_INDENT));
        if(mGraph.getBlock(id - 1).isJoin())
            graphString.append(String.format(
                    "bb%d [shape=record, label=\"<b>join\\nBB%d | {", id, id
            ));
        else
            graphString.append(String.format(
                "bb%d [shape=record, label=\"<b>BB%d | {", id, id
            ));
    }

    private void addInstruction(int id) {
        graphString.append(String.format("%d: ", id + 1));
        graphString.append(mGraph.getInstruction(id).getInst());
    }

    private void beginGraph() {
        graphString.append("digraph Program {");
    }

    private void endGraph() {
        graphString.append(branchString);
        graphString.append(domString);
        graphString.append("\n}\n");
    }

    public void print(PrintStream out) {
        out.print(graphString.toString());
    }
}
