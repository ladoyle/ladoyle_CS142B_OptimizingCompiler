package frontend.util;

import java.util.ArrayList;

public class Instruction {
    private final int id;
    private String left = "";
    private String right = "";
    private String inst = "";
    private final ArrayList<Integer> instructions;

    public Instruction(int id) {
        this.id = id;
        this.instructions = new ArrayList<>();
    }

    public int getID() {
        return this.id;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public boolean isEmptyInstruction() {
        return this.inst.equals("\\<empty\\>");
    }

    public boolean isBraInstruction() {
        return this.inst.matches("^bra.+");
    }

    public String getInst() {
        return this.inst;
    }

    public void insertInstruction(int id) {
        instructions.add(id);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof Instruction)) return false;

        Instruction in = (Instruction)obj;
        return this.inst.equals(in.inst);
    }

    void neg() { this.inst = "neg " + left; }
    void add() { this.inst = "add " + left + " " + right; }
    void sub() { this.inst = "sub " + left + " " + right; }
    void mul() { this.inst = "mul " + left + " " + right; }
    void div() { this.inst = "div " + left + " " + right; }
    void cmp() { this.inst = "cmp " + left + " " + right; }
//    private void adda(int x, int y) { this.inst = "adda (" + x + ") (" + y + ")\n";}
//    private void load(int x) { this.inst = "load (" + x + ")\n"; }
//    private void store(int x, int y) { this.inst = "store (" + x + ") (" + y + ")\n"; }
    void phi() { this.inst = "phi " + left + " " + right; }
//    private void end() { this.inst = "end" + "\n"; }
    void bra() { this.inst = "bra " + left; }
    void bne() { this.inst = "bne " + left; }
    void beq() { this.inst = "beq " + left; }
    void ble() { this.inst = "ble " + left; }
    void blt() { this.inst = "blt " + left; }
    void bge() { this.inst = "bge " + left; }
    void bgt() { this.inst = "bgt " + left; }
    void constant() { this.inst = "const " + left; }
    void empty() { this.inst = "\\<empty\\>"; }
}
