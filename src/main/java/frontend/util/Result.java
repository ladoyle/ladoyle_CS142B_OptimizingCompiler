package frontend.util;

public class Result {

    public enum ValueKind {
        Constant,
        Variable,
        Register
    }
    private int val;
    private int addr;
    private int reg;
    private final ValueKind kind;

    public Result(ValueKind newKind, int num) {
        switch (newKind) {
            case Constant: this.val = num; break;
            case Variable: this.addr = num; break;
            case Register: this.reg = num;
        }
        this.kind = newKind;
    }

    public int getNum() {
        switch (kind) {
            case Constant: return val;
            case Variable: return addr;
            default: return reg;
        }
    }

    public ValueKind getKind() {
        return kind;
    }
}
