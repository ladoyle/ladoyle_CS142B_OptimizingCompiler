package frontend.util;

import java.util.ArrayList;
import java.util.Collections;

public class SSAgraph {
    private final ArrayList<BasicBlock> mBlocks;
    private final ArrayList<Instruction> mInstructions;
    private int block_id = -1;
    private int inst_id = -1;

    public SSAgraph() {
        this.mBlocks = new ArrayList<>();
        this.mInstructions = new ArrayList<>();
    }

    public void addBlock(int numBlocks) {
        int temp_id = block_id + 1;
        for(int i = 0; i < numBlocks; ++i) {
            if(numBlocks > 1 && i == numBlocks - 1)
                mBlocks.add(new BasicBlock(true));
            else mBlocks.add(new BasicBlock(false));
            ++block_id;
            this.addInstruction(-1, null, null);
        }
        block_id = temp_id;
        if(block_id > 0) {
            for(int i = 0; i < numBlocks; ++i) {
                mBlocks.get(block_id - 1).connectBlock(block_id + i);
            }
        }
    }

    public int instructionExists(Instruction inst) {
        ArrayList<Instruction> revInstructions = new ArrayList<>(mInstructions);
        Collections.reverse(revInstructions);
        for(var instruction : revInstructions) {
            if(instruction.equals(inst)) return instruction.getID();
        }
        return -1;
    }

    public Instruction getTempInstruction(int opCode, Result x, Result y) {
        String left = lowerResult(x);
        String right = lowerResult(y);
        Instruction inst = new Instruction(-1);
        fillInstructionString(inst, opCode, left, right);
        return inst;
    }

    public int addInstruction(int opCode, Result x, Result y) {
        // if last instruction was <empty> just replace
        int firstInstructionID = mBlocks.get(block_id).getFirstInstruction();
        if(firstInstructionID >= 0 &&
                mInstructions.get(firstInstructionID).isEmptyInstruction()) {
            changeInstruction(
                    2, mInstructions.get(firstInstructionID), opCode, x, y
            );
            return firstInstructionID;
        }

        String left = lowerResult(x);
        String right = lowerResult(y);
        ++inst_id;
        mBlocks.get(block_id).insertInstruction(inst_id);
        mInstructions.add(new Instruction(inst_id));
        fillInstructionString(
                mInstructions.get(inst_id),
                opCode,
                left, right
        );
        return inst_id;
    }

    public void changeInstruction(
            int num, Instruction inst, int opCode, Result x, Result y) {
        String left;
        String right;
        switch (num) {
            case 0: left = lowerResult(x); right = inst.getRight(); break;
            case 1: left = inst.getLeft(); right = lowerResult(y); break;
            default: left = lowerResult(x); right = lowerResult(y);
        }
        fillInstructionString(
                inst,
                opCode,
                left, right
        );
    }

    private void fillInstructionString(
            Instruction inst, int opCode, String left, String right) {
        inst.setLeft(left);
        inst.setRight(right);
        switch (opCode) {
            case -1: inst.empty(); break;
            case 0: inst.add(); break;
            case 1: inst.sub(); break;
            case 2: inst.mul(); break;
            case 3: inst.div(); break;
            case 5: inst.cmp(); break;
//            case "adda": currentBlock.instructions.add(new Instruction());
//                        currentBlock.instructions.get(index).adda(x, y); break;
//            case "load": currentBlock.instructions.add(new Instruction());
//                        currentBlock.instructions.get(index).load(x); break;
//            case "store": currentBlock.instructions.add(new Instruction());
//                        currentBlock.instructions.get(index).store(x, y); break;
//            case "end": currentBlock.instructions.add(new Instruction());
//                        currentBlock.instructions.get(index).end(); break;
            case 39: inst.bra(); break;
            case 41: inst.bne(); break;
            case 40: inst.beq(); break;
            case 44: inst.ble(); break;
            case 42: inst.blt(); break;
            case 43: inst.bge(); break;
            case 45: inst.bgt(); break;
            case 11: inst.constant(); break;
            case 12: inst.neg(); break;
            case 13: inst.phi(); break;
        }
    }

    private String lowerResult(Result res) {
        if(res == null) return "";
        switch(res.getKind()) {
            case Constant: return "#" + res.getNum();
            case Register: return "$" + res.getNum();
            default: return "(" + (res.getNum() + 1) + ")";
        }
    }

    public int getNumBlocks() {
        return this.mBlocks.size();
    }

    public void switchBlock(int id) {
        block_id = id;
    }

    public BasicBlock getBlock(int id) {
        return this.mBlocks.get(id);
    }

    public Instruction getInstruction(int id) {
        return this.mInstructions.get(id);
    }

    public int getInstID() {
        return inst_id;
    }

    public int getBlockID() {
        return block_id;
    }
}
