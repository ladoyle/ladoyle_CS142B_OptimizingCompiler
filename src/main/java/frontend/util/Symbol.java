package frontend.util;

public class Symbol {
    String sym;
    int id;
    int val;

    public Symbol(String sym, int id, int val) {
        this.sym = sym;
        this.id = id;
        this.val = val;
    }

    public String getSym() {
        return sym;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(getClass() != obj.getClass()) return false;

        Symbol symbol = (Symbol) obj;
        return this.sym.equals(symbol.sym);
    }
}
