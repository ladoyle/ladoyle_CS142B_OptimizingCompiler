import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.DynamicTest.dynamicTest;

class CompilerTest {
    private static final PrintStream out = System.out;
    private static final PrintStream err = System.err;
    private static ByteArrayOutputStream outputStream;
    private static ByteArrayOutputStream errorStream;

    @TestFactory
    Stream<DynamicTest> tokenize() throws IOException {
        var tests = getTests("lexerStage");
        return tests.stream().map(test -> dynamicTest(test.in, () -> {
            var driver = new Driver(test.in);
            driver.setLexer();

            var status = driver.run();
            var actualOutput = status == State.Finished ? outputStream.toString() : errorStream.toString();

            outputStream.reset();
            errorStream.reset();
            var expectedOutput = readResourceToString(test.out);
            Assertions.assertEquals(expectedOutput, actualOutput);
        }));
    }

    @TestFactory
    Stream<DynamicTest> parse() throws IOException {
        var tests = getTests("parserStage");
        return tests.stream().map(test -> dynamicTest(test.in, () -> {
            var driver = new Driver(test.in);
            driver.setParser();

            var status = driver.run();
            var actualOutput = status == State.Finished ? outputStream.toString() : errorStream.toString();

            outputStream.reset();
            errorStream.reset();
            var expectedOutput = readResourceToString(test.out);
            Assertions.assertEquals(expectedOutput, actualOutput);
        }));
    }

    @BeforeAll
    static void setOutputStreams() {
        System.setOut(new PrintStream(outputStream = new ByteArrayOutputStream()));
        System.setErr(new PrintStream(errorStream = new ByteArrayOutputStream()));
    }

    @AfterAll
    static void resetOutputStreams() {
        System.setOut(out);
        System.setErr(err);
    }

    private List<InOut> getTests(String stageName) throws IOException {
        var loader = getClass().getClassLoader();
        var folder = String.format("%s", stageName);
        try (var programs = loader.getResourceAsStream(folder);
             BufferedReader br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(programs)))) {

            return br.lines()
                    .filter(resourceName -> resourceName.endsWith(".crx"))
                    .map(resourceName -> {
                        var testName = resourceName.substring(0, resourceName.length() - 4);
                        var input = String.format("%s/%s.in", folder, testName);
                        var out = String.format("%s/%s.out", folder, testName);
                        return new InOut(folder + "/" + resourceName, input, out);
                    }).collect(Collectors.toList());
        }
    }

    private String readResourceToString(String resourceName) throws IOException {
        var loader = getClass().getClassLoader();
        try (var inputStream = Objects.requireNonNull(loader.getResourceAsStream(resourceName))) {
            var result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString(StandardCharsets.UTF_8);
        }
    }

    private static final class InOut {
        final String in;
        final String input;
        final String out;

        private InOut(String in, String input, String out) {
            this.in = in;
            this.input = input;
            this.out = out;
        }
    }
}